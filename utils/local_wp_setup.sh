#!/bin/sh

wdir=/Users/mskeaton/www/iwamototosou.com/wp-content/themes/iwamototosou
test -d ${wdir} || mkdir -p ${wdir}
cdir=$(cd $(dirname $0);pwd)
rm -rf ${wdir}/*
cp -a ${cdir}/../wp/* ${wdir}
chgrp -R staff ${wdir}/*
chmod -R 775 ${wdir}/*

