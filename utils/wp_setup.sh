#!/bin/sh

wdir=/var/www/iwamototosou.com/wp-content/themes/iwamototosou
test -d ${wdir} || mkdir -p ${wdir}
cdir=$(cd $(dirname $0);pwd)
rm -rf ${wdir}/*
cp -a ${cdir}/../wp/* ${wdir}
chgrp -R www-data ${wdir}/*
chmod -R 775 ${wdir}/*
