#coding:utf-8
#wsgi0.py

import Cookie
import os, sys, codecs
import cgi
import json
import iwmyfunc as mf

import re

import cgitb
cgitb.enable()

reload(sys)
sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
sys.setdefaultencoding('utf-8')


DOCROOT = '/var/www/iwamototosou.com'

def application(env, start_response):
  session = ''
  post = ''
  debug = ''
  cookie = Cookie.SimpleCookie()

  if env.has_key('HTTP_COOKIE'):
    cookie.load(env['HTTP_COOKIE'])

  if env['REQUEST_METHOD'] == 'POST':
    wsgi_input = env['wsgi.input']
    post = cgi.FieldStorage(fp=wsgi_input,environ=env,keep_blank_values=True)
    print post

  path = env['PATH_INFO']

  slugs = ['about','works','flow','news']

  status = '200 OK'

  if path == '/':
    output = mf.getHtml('top')

  elif path == '/login/':
    output = '[{"message":"login_ng"}]'
    if post.has_key('user_name') and post.has_key('user_pass'):
      user_name = post['user_name'].value
      user_pass = post['user_pass'].value
      message = mf.login(user_name, user_pass)
      output = json.dumps( {'message':message, 'session_id':mf.getSessionId()} )
    response_headers = [('Content-type', 'application/json'),('Content-Length', str(len(output)))]

  elif path == '/admin/':
    if cookie.has_key('session_id') and cookie['session_id'].value != '':
      output = mf.getAdminHtml()
    else:
      output = mf.getLoginHtml()

  elif re.match('/news/[0-9]+/', path) is not None:
    id = re.search('[0-9]+', path).group(0)
    output = mf.getHtmlByPostId('newssingle', id)

  elif re.match('/works/[0-9]+/', path) is not None:
    id = re.search('[0-9]+', path).group(0)
    output = mf.getHtmlByPostId('workssingle', id)

  elif path.replace('/','') in slugs:
    output = mf.getHtml(path.replace('/',''))

  elif path == '/sitemap.txt':
    output = mf.getSitemap()

  elif path == '/send_mail/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    name = jsond['name'];
    email = jsond['email'];
    tel = jsond['tel'];
    address = jsond['address'];
    content = jsond['content'];
    from_address = 'contact@iwamototosou.com'
    to_address_li = ['iwamototosou.com@gmail.com']
    to_address_li2 = ['info@iwamototosou.com', 'info@cb-web.com']
    subject = name + u'様からのお問い合わせ'
    info = u'[お名前] ' + name + '\n'
    info += u'[E-mail] ' + email + '\n'
    info += u'[TEL] ' + tel + '\n'
    info += u'[住所] ' + address + '\n'
    info += u'[お問い合わせ内容]\n' + content + '\n'
    body = u'ホームページから問い合わせがありました。\n\n' + info
    mf.sendMail(from_address, to_address_li, subject, body)
    mf.sendMail(from_address, to_address_li2, subject, body)
    output = str(info)

  elif path == '/add_news/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    paths = mf.saveImagesForAdd(DOCROOT, jsond['imgfilename'], jsond['imgdata'], jsond['otherimages'])
    id = mf.addPostNews(jsond['title'], paths['imgpath'], jsond['text'], paths['otherpaths'])
    output = ','.join([str(id), jsonstr])

  elif path == '/add_works/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    paths = mf.saveImagesForAdd(DOCROOT, jsond['imgfilename'], jsond['imgdata'], jsond['otherimages'])
    paths2 = mf.saveImagesForAdd(DOCROOT, jsond['imgfilename2'], jsond['imgdata2'], '')
    id = mf.addPostWorks(jsond['title'], paths['imgpath'], jsond['text'], paths['otherpaths'], jsond['month'], jsond['category'], paths2['imgpath'])
    output = ','.join([str(id), jsonstr])

  elif path == '/edit_news/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    paths = mf.saveImagesForEdit(DOCROOT, jsond['imgpath'], jsond['imgfilename'], jsond['imgdata'], jsond['otherimages'])
    id = mf.editPostNews(jsond['id'], jsond['title'], paths['imgpath'], jsond['text'], paths['otherpaths'])
    output = ','.join([str(id), jsonstr])

  elif path == '/edit_works/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    paths = mf.saveImagesForEdit(DOCROOT, jsond['imgpath'], jsond['imgfilename'], jsond['imgdata'], jsond['otherimages'])
    paths2 = mf.saveImagesForEdit(DOCROOT, jsond['imgpath2'], jsond['imgfilename2'], jsond['imgdata2'], '')
    id = mf.editPostWorks(jsond['id'], jsond['title'], paths['imgpath'], jsond['text'], paths['otherpaths'], jsond['month'], jsond['category'], paths2['imgpath'])
    output = ','.join([str(id), jsonstr])

  elif path == '/get_news_as_table/':
    output = mf.getPostNewsTable()

  elif path == '/get_works_as_table/':
    output = mf.getPostWorksTable()

  elif path == '/delete_news/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    id = jsond['id']
    mf.removePostNews(id)
    output = ','.join([str(id), jsonstr])

  elif path == '/delete_works/':
    jsonstr = post['json'].value
    jsond = json.loads(jsonstr)
    id = jsond['id']
    mf.removePostWorks(id)
    output = ','.join([str(id), jsonstr])

  else:
    status = '404 Not Found'
    output = '404 Not Found'

  response_headers = [
    ('Content-type', 'text/html'),
    ('Content-Length', str(len(output))),
  ]

  start_response(status, response_headers)

  return [output]


