#coding:utf-8
#myfunc.py

import os, sys, codecs
reload(sys)
#sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
sys.stdout = sys.stderr
sys.setdefaultencoding('utf-8')

import sqlite3
import datetime
import base64
import smtplib
from email.MIMEText import MIMEText
from email.Header import Header
from email.Utils import formatdate

DOCROOT = os.path.dirname(os.path.abspath(__file__)) + '/'
DBNAME = '/var/www/iwamototosou.com/database.db'
#DBNAME = 'database.db'

def sendMail(from_address, to_address_li, subject, body):
  charset = 'ISO-2022-JP'
  msg = MIMEText(body.encode(charset), 'plain', charset)
  msg['Subject'] = Header(subject, charset)
  msg['From'] = from_address
  msg['To'] = ','.join(to_address_li)
  msg['Date'] = formatdate(localtime=True)
  smtp = smtplib.SMTP('smtp14.gmoserver.jp', 587)
  smtp.login('info@cb-web.com', '1#rTreU&')
  smtp.sendmail(from_address, to_address_li, msg.as_string())
  smtp.close()
  print str(to_address_li) + '\n' + subject + '\n' + body
  return str(to_address_li) + '\n' + subject + '\n' + body

def getSitemap():
  f_html = open(DOCROOT + 'sitemap.txt')
  html = f_html.read()
  f_html.close()
  return html

def getHtml(slug):
  f_header_html = open(DOCROOT + 'header.html')
  header_html = f_header_html.read()
  f_header_html.close()
  
  f_footer_html = open(DOCROOT + 'footer.html')
  footer_html = f_footer_html.read()
  f_footer_html.close()

  f_main_html = open(DOCROOT + 'main_' + slug + '.html')
  main_html = f_main_html.read()
  f_main_html.close()
  
  html = header_html + main_html + footer_html
  if slug == 'works':
    html = html.replace('@@@post_works_list@@@', getPostWorksListHtml())
  html = html.replace('@@@posts_footer@@@', getPostsUlForFooter())
  html = html.replace('@@@posts_page@@@', getPostsUl())
  html = html.replace('@@@access_num@@@', getAccessNum())

  return html

def getHtmlByPostId(slug, id):
  f_header_html = open(DOCROOT + 'header.html')
  header_html = f_header_html.read()
  f_header_html.close()
  
  f_footer_html = open(DOCROOT + 'footer.html')
  footer_html = f_footer_html.read()
  f_footer_html.close()

  f_main_html = open(DOCROOT + 'main_' + slug + '.html')
  main_html = f_main_html.read()
  f_main_html.close()
  
  html = header_html + main_html + footer_html
  if slug == 'workssingle':
    html = html.replace('@@@post_works_single@@@', getPostWorksSingleHtml(id))
  html = html.replace('@@@post_single@@@', getPostsUlForSingle(id))
  html = html.replace('@@@posts_footer@@@', getPostsUlForFooter())
  html = html.replace('@@@posts_page@@@', getPostsUl())
  html = html.replace('@@@access_num@@@', getAccessNum())

  return html

def getAdminHtml():
  f_main_html = open(DOCROOT + 'main_admin.html')
  main_html = f_main_html.read()
  f_main_html.close()
  return main_html

def getLoginHtml():
  f_main_html = open(DOCROOT + 'main_login.html')
  main_html = f_main_html.read()
  f_main_html.close()
  return main_html

def login(user_name, user_pass):
  result = 'login_ng'
  if checkUserExistsByName(user_name):
    if selectUserpassByName(user_name) == user_pass:
      result = 'login_ok'
  return result

def checkUserExistsByName(name):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  result = False
  sql = 'select count(*) from users where name=?'
  for row in c.execute(sql, (name,)):
    if int(row[0]) > 0:
      result = True
  conn.close()
  return result

def getSessionId():
  return 'session_id_dummy'

def selectUserpassByName(name):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  password = ''
  sql = 'select pass from users where name=?'
  for row in c.execute(sql, (name,)):
    password = row[0]
  conn.close()
  return password
  
def getNewsMainHtml():
  return 'html'

def getHtmlFromTemplate(template):
  code_newslist = '[[news]]'
  html_newslist = '<ul class="news">'
  
  return 'html'

'''
js ログイン
js 編集モード
js 編集モードで記事クリック->sidepane表示
js sidepaneで編集 画像はドラッグ＆ドロップorカメラロール
  一旦ローカルの画像を表示しておいて、更新するときにアップロードする
  編集時はサーバのファイルを表示する。追加でドロップした画像はローカルのファイルを表示。
  textarea内に画像を表示できるか
js sidepane で編集、更新->ajaxでhtmlとimg送信
py img受信、ファイル保存
py html受信、db更新
同期通信img送信->img受信、保存->html送信->html受信
img upload/year/month/date/time.png dbで関連付け。img_id(post_datetime(id) - path)
class Post > News, Post > Works
a piece of news???
ページごとのリソース（文字列、画像）をDBに突っ込む 全部にIDつける
  contentEditable 文字列はこれでいいか。画像は？ふわっと編集ボタンでファイル選択
編集モード移行時に編集前のリソース情報を保管。更新実行時に差分を検索して同意を得る。
スタイル編集はとりあえずやらない。
管理者ログインのリンクはとりあえず見せちゃう。lightboxログイン->画面更新ならばまあ、セキュアか？？という感じ
ログインじにサーバからハッシュを発行してcookieで保存
'''

# @common

def convert_b64_string_to_file(s, outfile_path):
  """base64をデコードしてファイルに書き込む
  """
  with open(outfile_path, "wb") as f:
    f.write(base64.b64decode(base64.b64decode(s).split(',')[1]))

def dict_factory(cursor, row):
  d = {}
  for idx, col in enumerate(cursor.description):
    d[col[0]] = row[idx]
  return d

def getNextIdWithCursor(c, table_name):
  count = 0
  maxid = 0
  count_sql = 'select count(*) from ' + table_name
  maxid_sql = 'select max(id) from ' + table_name
  for row in c.execute(count_sql):
    count = row[0]
  if int(count) != 0:
    for row in c.execute(maxid_sql):
      maxid = row[0]
  return maxid + 1

def getNextId(table_name):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  count = 0
  maxid = 0
  count_sql = 'select count(*) from ' + table_name
  maxid_sql = 'select max(id) from ' + table_name
  for row in c.execute(count_sql):
    count = row[0]
  if int(count) != 0:
    for row in c.execute(maxid_sql):
      maxid = row[0]
  conn.commit()
  conn.close()
  return maxid + 1

def getNowStr():
  now = datetime.datetime.now()
  return '{0:%Y%m%d%H%M%S}'.format(now)

def formatDatetimeStr(datetimestr):
  y = datetimestr[0:4]
  m = datetimestr[4:6]
  d = datetimestr[6:8]
  H = datetimestr[8:10]
  M = datetimestr[10:12]
  S = datetimestr[12:]
  return '/'.join([y, m, d]) + ' ' + ':'.join([H, M, S])

def getDateStr(datetimestr, separator):
  y = str(int(datetimestr[0:4]))
  m = str(int(datetimestr[4:6]))
  d = str(int(datetimestr[6:8]))
  return separator.join([y, m, d])

def getStr20(str):
  sstr = str
  if len(sstr) > 20:
    sstr = sstr[:16] + ' ...'
  return sstr

def select(table_name):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  select_sql = 'select * from ' + table_name
  rows = c.execute(select_sql)
  for row in rows:
    print(row)
  conn.close()
  return rows

def initUsers():
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  create_table = (
    'create table users ('
    '  id int,'
    '  name varchar(64),'
    '  pass varchar(64),'
    '  jpname varchar(64),'
    '  category varchar(64),'
    '  email varchar(64)'
    ')'
  )
  c.execute(create_table)
  insert_sql = 'insert into users (id, name, pass, jpname, category, email) values(?, ?, ?, ?, ?, ?)'
  users = [
    (0, 'admin', 'mspublic1029', u'管理', 'admin', 'info@cb-web.com')
  ]
  c.executemany(insert_sql, users)
  conn.commit()
  select_sql = 'select * from users'
  for row in c.execute(select_sql):
    print(row)
  conn.close()



# @works

def saveImage(docroot, imgfilename, imgdata):
  imgpath = '/static/uploads/' + imgfilename
  if os.path.exists( (docroot + '/' + imgpath).encode('utf-8') ):
    imgpath = '/static/uploads/' + getNowStr() + '_' + imgfilename
  convert_b64_string_to_file(imgdata, (docroot + '/' + imgpath).encode('utf-8'))
  return imgpath

def saveImagesForAdd(docroot, imgfilename, imgdata, otherimages):
  paths = {}
  imgpath = ''
  otherpaths = []
  if imgfilename != '':
    imgpath = saveImage(docroot, imgfilename, imgdata)
  if otherimages != '':
    for imginfo in otherimages:
      otherpath = saveImage(docroot, imginfo['name'], imginfo['data'])
      otherpaths.append(otherpath)
  paths['imgpath'] = imgpath
  paths['otherpaths'] = otherpaths
  return paths

def saveImagesForEdit(docroot, oldpath, imgfilename, imgdata, otherimages):
  paths = {}
  imgpath = ''
  otherpaths = []
  if imgfilename != '':
    imgpath = saveImage(docroot, imgfilename, imgdata)
  else:
    imgpath = oldpath
  if otherimages != '':
    for imginfo in otherimages:
      otherpath = ''
      if imginfo['name'] != '':
        otherpath = saveImage(docroot, imginfo['name'], imginfo['data'])
      else:
        otherpath = imginfo['path']
      otherpaths.append(otherpath)
  paths['imgpath'] = imgpath
  paths['otherpaths'] = otherpaths
  return paths



def initModel(table_name, columns):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  l = list()
  for column in columns:
    l.append(column['name'] + ' ' + column['type'])
  create_table = 'create table ' + table_name + ' (' + ','.join(l) + ')'
  print create_table
  c.execute(create_table)

def initPostExtColumns():
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  create_table = (
    'create table post_ext_columns ('
    '  id int,'
    '  post_slug text,'
    '  column_name text,'
    '  column_type text,'
    '  updated_at text,'
    '  created_at text'
    ')'
  )
  c.execute(create_table)

def initPostExtDatas():
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  create_table = (
    'create table post_ext_datas ('
    '  id int,'
    '  post_slug text,'
    '  column_id int,'
    '  data text,'
    '  updated_at text,'
    '  created_at text'
    ')'
  )
  c.execute(create_table)

def create(table_name, columns, types):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  columntypes = []
  for column,type in zip(columns,types):
    columntypes += [str(column) + ' ' + str(type)]
  columntypes = ['id int'] + columntypes + ['updated_at text', 'created_at text']
  sql = 'create table if not exists ' + table_name + ' ('
  sql += ','.join(columntypes)
  sql += ')'
  c.execute(sql)

def drop(table_name):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  sql = 'drop table ' + table_name
  c.execute(sql)

def insert(table_name, columns, values):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  id = getNextIdWithCursor(c, table_name)
  updated_at = getNowStr()
  created_at = getNowStr()
  columns = ['id'] + columns + ['updated_at', 'created_at']
  questions = []
  for i in range(len(columns)):
    questions += ['?']
  values = [id] + values + [updated_at, created_at]
  sql = 'insert into ' + table_name + '(' + ','.join(columns) + ') values(' + ','.join(questions) + ')'
  print sql
  print values
  c.execute(sql, values)
  conn.commit()
  conn.close()

def update(table_name, where, columns, values):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  updated_at = getNowStr()
  columnquestions = []
  for column in columns:
    columnquestions += [str(column) + '=?']
  columnquestions += ['updated_at=?']
  values += [updated_at]
  sql = 'update ' + table_name + ' set ' + ','.join(columnquestions) + ' where ' + where
  print sql
  print values
  c.execute(sql, values)
  conn.commit()
  conn.close()

def delete(table_name, where):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  sql = 'delete from ' + table_name
  if where != '':
    sql += ' where ' + where
  print sql
  c.execute(sql)
  conn.commit()
  conn.close()

def selectOrder(table_name, order):
  conn = sqlite3.connect(DBNAME)
  conn.row_factory = dict_factory
  c = conn.cursor()
  sql = 'select * from ' + table_name + ' order by ' + order
  rows = []
  for row in c.execute(sql):
    rows += [row]
  conn.close()
  return rows

def selectWhere(table_name, where):
  conn = sqlite3.connect(DBNAME)
  conn.row_factory = dict_factory
  c = conn.cursor()
  sql = 'select * from ' + table_name + ' where ' + where
  rows = []
  for row in c.execute(sql):
    rows += [row]
  conn.close()
  return rows

def selectJoinWhereOrder(main_table_name, main_table_key, main_table_columns, join_tables, as_columns, where, order):
  mtn = main_table_name
  mtk = main_table_key
  columnstrs = []
  joinstrs = []
  for mtc in main_table_columns:
    columnstrs += [mtn + '.' + mtc]
  for jt in join_tables:
    jtn = jt['name']
    for jtc in jt['columns']:
      columnstrs += [jtn + '.' + jtc]
    joinstrs += ['inner join ' + jtn + ' on ' + mtn + '.' + mtk + '=' + jtn + '.' + jt['key']]
  for i, cs in enumerate(columnstrs):
    columnstrs[i] = cs + ' as ' + as_columns[i]
  conn = sqlite3.connect(DBNAME)
  conn.row_factory = dict_factory
  c = conn.cursor()
  sql = 'select ' + ','.join(columnstrs) + ' from ' + mtn + ' ' + ' '.join(joinstrs)
  if where != '':
    sql += ' where ' + where
  if order != '':
    sql += ' order by ' + order
  print sql
  rows = []
  for row in c.execute(sql):
    rows += [row]
  conn.close()
  return rows

def selectJoinWhere(main_table_name, main_table_key, main_table_columns, join_tables, as_columns, where):
  return selectJoinWhereOrder(main_table_name, main_table_key, main_table_columns, join_tables, as_columns, where, '')

def selectJoinOrder(main_table_name, main_table_key, main_table_columns, join_tables, as_columns, order):
  return selectJoinWhereOrder(main_table_name, main_table_key, main_table_columns, join_tables, as_columns, '', order)

def selectJoin(main_table_name, main_table_key, main_table_columns, join_tables, as_columns):
  return selectJoinOrder(main_table_name, main_table_key, main_table_columns, join_tables, as_columns, '')


def initPostWorks():
  columns = ['post_id', 'month', 'category', 'before_img_path']
  types = ['int', 'text', 'text', 'text']
  create('post_works', columns, types)

def initPostNews():
  columns = ['post_id']
  types = ['int']
  create('post_news', columns, types)

def addPostWorks(title, imgpath, text, otherpath_list, month, category, before_img_path):
  post_id = insertPost(title, imgpath, text, otherpath_list)
  insert('post_works', ['post_id', 'month', 'category', 'before_img_path'], [post_id, month, category, before_img_path])
  return post_id

def addPostNews(title, imgpath, text, otherpath_list):
  post_id = insertPost(title, imgpath, text, otherpath_list)
  insert('post_news', ['post_id'], [post_id])
  return post_id

def editPostWorks(post_id, title, imgpath, text, otherpath_list, month, category, before_img_path):
  post_id = updatePost(post_id, title, imgpath, text, otherpath_list)
  update('post_works', 'post_id=' + str(post_id), ['month', 'category', 'before_img_path'], [month, category, before_img_path])
  return post_id

def editPostNews(post_id, title, imgpath, text, otherpath_list):
  post_id = updatePost(post_id, title, imgpath, text, otherpath_list)
  update('post_news', 'post_id=' + str(post_id), [], [])
  return post_id

def removePostWorks(post_id):
  delete('post_works', 'post_id=' + str(post_id))
  delete('image_paths', 'post_id=' + str(post_id))
  delete('posts', 'id=' + str(post_id))
  return post_id

def removePostNews(post_id):
  delete('post_news', 'post_id=' + str(post_id))
  delete('image_paths', 'post_id=' + str(post_id))
  delete('posts', 'id=' + str(post_id))
  return post_id

def getPostWorksSingleHtml(id):
  columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  post_works = {'name':'post_works', 'key':'post_id', 'columns':['month', 'category', 'before_img_path']}
  as_columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at', 'month', 'category', 'before_img_path']
  rows = selectJoinWhere('posts', 'id', columns, [post_works], as_columns, 'posts.id=' + str(id))
  html = ''
  for row in rows:
    html += '<div class="main">'
    html += '<h2 class="delay">[' + str(row['title']) + ']</h2>'
    html += '<table>'
    if 'before_img_path' in row and str(row['before_img_path']) != '':
      html += '<tr><th>[Before]</th><th></th><th>[After]</th></tr>'
    else:
      html += '<tr><th></th><th></th><th></th></tr>'
    html += '<tr>'
    if 'before_img_path' in row and str(row['before_img_path']) != '':
      html += '<td><a href="' + str(row['before_img_path']) + '"><img src="' + str(row['before_img_path']) + '"></a></td>'
      html += '<td><img src="/static/img/works/arrow.png"></td>'
      if str(row['imgpath']) != '':
        html += '<td><a href="' + str(row['imgpath']) + '"><img src="' + str(row['imgpath']) + '"></a></td>'
      else:
        html += '<td></td>'
    else:
      html += '<td></td>'
      if str(row['imgpath']) != '':
        html += '<td class="only"><a href="' + str(row['imgpath']) + '><img src="' + str(row['imgpath']) + '"></a></td>'
      else:
        html += '<td></td>'
      html += '<td></td>'
    html += '</tr>'
    html += '</table>'
    html += '</div>'
    html += '<div class="photos">'
    html += '<h3>[PHOTO]</h3>'
    html += '<ul>'
    html += getImagesLiWithPostId(row['id'])
    html += '</ul>'
    html += '</div>'
    html += '<div class="data">'
    html += '<h3>[ 施工データ ]</h3>'
    html += '<span class="title">' + str(row['title']) + '</span><br>'
    html += '<span class="month">' + str(row['month']) + '</span>'
    html += '<span class="category">' + str(row['category']) + '</span><br>'
    html += '<span class="text">' + str(row['text']) + '</span>'
    html += '</div>'
  return html

def getPostWorksListHtml():
  columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  post_works = {'name':'post_works', 'key':'post_id', 'columns':['month', 'category', 'before_img_path']}
  as_columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at', 'month', 'category', 'before_img_path']
  rows = selectJoinOrder('posts', 'id', columns, [post_works], as_columns, 'id desc')
  html = '<ul>'
  for row in rows:
    html += '<a href="/works/' + str(row['id']) + '/">'
    html += '<li>'
    if str(row['imgpath']) != '':
      html += '<span class="img"><img src="' + str(row['imgpath']) + '"></span>'
    html += '<span class="title">' + str(row['title']) + '</span>'
    html += '<span class="month">' + str(row['month']) + '</span>'
    html += '<span class="category">' + str(row['category']) + '</span>'
    html += '<span class="text">' + str(row['text']) + '</span>'
    html += '</li>'
    html += '</a>'
  html += '</ul>'
  return html

def getPostWorksTable():
  columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  post_works = {'name':'post_works', 'key':'post_id', 'columns':['month', 'category', 'before_img_path']}
  as_columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at', 'month', 'category', 'before_img_path']
  rows = selectJoinOrder('posts', 'id', columns, [post_works], as_columns, 'id desc')
  print rows
  return getPostsTable(rows, 'works')

def getPostNews():
  return select('post_news')

def getPostNewsTable():
  columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  post_news = {'name':'post_news', 'key':'post_id', 'columns':[]}
  as_columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  rows = selectJoinOrder('posts', 'id', columns, [post_news], as_columns, 'id desc')
  print rows
  return getPostsTable(rows, 'news')


# @posts

def initPosts():
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  create_table = (
    'create table posts ('
    '  id int,'
    '  title text,'
    '  imgpath text,'
    '  text text,'
    '  updated_at text,'
    '  created_at text'
    ')'
  )
  c.execute(create_table)

def initImagePaths():
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  create_table = (
    'create table image_paths ('
    '  id int,'
    '  post_id int,'
    '  imgpath text,'
    '  updated_at text,'
    '  created_at text'
    ')'
  )
  c.execute(create_table)

def insertPost(title, imgpath, text, otherpath_list):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  id = getNextIdWithCursor(c, 'posts')
  updated_at = getNowStr()
  created_at = getNowStr()
  sql = 'insert into posts (id, title, imgpath, text, updated_at, created_at) values(?, ?, ?, ?, ?, ?)'
  post = (id, title, imgpath, text, updated_at, created_at)
  c.execute(sql, post)
  post_id = id
  for imgpath in otherpath_list:
    id = getNextIdWithCursor(c, 'image_paths')
    updated_at = getNowStr()
    created_at = getNowStr()
    sql = 'insert into image_paths (id, post_id, imgpath, updated_at, created_at) values(?, ?, ?, ?, ?)'
    values = (id, post_id, imgpath, updated_at, created_at)
    c.execute(sql, values)
  conn.commit()
  conn.close()
  return post_id

def updatePost(id, title, imgpath, text, otherpath_list):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  updated_at = getNowStr()
  sql = 'update posts set title=?, imgpath=?, text=?, updated_at=? where id=?'
  post = (title, imgpath, text, updated_at, id)
  c.execute(sql, post)
  post_id = id
  sql = 'delete from image_paths where post_id=?'
  c.execute(sql, [post_id])
  for imgpath in otherpath_list:
    id = getNextIdWithCursor(c, 'image_paths')
    updated_at = getNowStr()
    created_at = getNowStr()
    sql = 'insert into image_paths (id, post_id, imgpath, updated_at, created_at) values(?, ?, ?, ?, ?)'
    values = (id, post_id, imgpath, updated_at, created_at)
    c.execute(sql, values)
  conn.commit()
  conn.close()
  return post_id

def getPosts():
  columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  post_news = {'name':'post_news', 'key':'post_id', 'columns':[]}
  as_columns = ['id', 'title', 'imgpath', 'text', 'updated_at', 'created_at']
  rows = selectJoinOrder('posts', 'id', columns, [post_news], as_columns, 'id desc')
  return rows

def getPostLi(row):
  html = '<li>'
  html += '<a href="/news/' + str(row['id']) + '/">'
  html += '<div class="head">'
  html += '<span class="date">' + getDateStr(str(row['created_at']), '.') + '</span>'
  html += '<span class="title">' + row['title'].encode('UTF-8') + '</span>'
  html += '<span class="category">ニュース</span>'
  html += '</div>'
  html += '</a>'
  html += '<div class="body">'
  if str(row['imgpath']) != '':
    html += '<img src="' + str(row['imgpath']) + '"/>'
  html += '<p>' + str(row['text']).replace('\n', '<br>\n') + '</p>'
  html += getImagesHtmlWithPostId(row['id'])
  html += '</div>'
  html += '</li>'
  return html

def getPostsUl():
  rows = getPosts()
  html = '<ul>'
  for row in rows:
    html += getPostLi(row)
  html += '</ul>'
  return html
  
def getPostsUlForFooter():
  rows = getPosts()
  cnt = 1;
  cnt_max = 5;
  html = '<ul>'
  for row in rows:
    html += '<li>'
    html += '<a href="/news/' + str(row['id']) + '/">'
    html += '<span class="date">' + getDateStr(str(row['created_at']), '.') + '</span>'
    html += '<span class="title">' + row['title'].encode('UTF-8') + '</span>'
    html += '<span class="category">ニュース</span>'
    html += '</a>'
    html += '</li>'
    cnt += 1
    if cnt > cnt_max:
      break
  html += '</ul>'
  return html
  
def getPostsUlForSingle(id):
  rows = selectWhere('posts', 'id=' + str(id))
  html = '<ul>'
  for row in rows:
    html += getPostLi(row)
  html += '</ul>'
  return html

def getPostsTable(rows, mode):
  html = '<table class="' + mode + '">'
  html += '<tr><th>No.</th><th>記事</th><th>更新日</th><th>作成日</th></tr>'
  for row in rows:
    print rows
    print row
    pathrows = getImagePathsWithPostId(row['id'])
    paths_html = ''
    for pathrow in pathrows:
      paths_html += '<img class="otherimg" style="display:none" src="' + str(pathrow['imgpath']) + '">'
    if 'before_img_path' in row:
      paths_html += '<img class="beforeimg" style="display:none" src="' + str(row['before_img_path']) + '">'
    html += '<tr class="news_row">'
    html += '<td>'
    html += '<span class="id">' + str(row['id']) + '</span>'
    html += '</td>'
    html += '<td class="post">'
    html += '<img class="img" src="' + str(row['imgpath']) + '"/>'
    html += paths_html
    html += '<span class="title">' + str(row['title']) + '</span>'
    if 'month' in row:
      html += '<span class="month">' + str(row['month']) + '</span>'
    if 'category' in row:
      html += '<span class="category">' + str(row['category']) + '</span>'
    html += '<span class="text">' + str(row['text']) + '</span>'
    html += '</td>'
    html += '<td>'
    html += '<span class="updated_at">' + formatDatetimeStr(str(row['updated_at'])) + '</span>'
    html += '</td>'
    html += '<td>'
    html += '<span class="created_at">' + formatDatetimeStr(str(row['created_at'])) + '</span>'
    html += '</td>'
    html += '</tr>'
  html += '</table>'
  return html

  
def deletePost(id):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()

  sql = 'delete from posts where id=?'
  c.execute(sql, [id])
  conn.commit()
  conn.close()

  return id

def deleteImagePathsWithPostId(post_id):
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()

  sql = 'delete from image_paths where post_id=?'
  c.execute(sql, [post_id])
  conn.commit()
  conn.close()

  return post_id


def getImagePathsWithPostId(post_id):
  conn = sqlite3.connect(DBNAME)
  conn.row_factory = dict_factory
  c = conn.cursor()
  select_sql = 'select * from image_paths where post_id=' + str(post_id)
  rows = []
  for row in c.execute(select_sql):
    rows += [row]
  conn.close()
  return rows

def getImagesHtmlWithPostId(post_id):
  rows = getImagePathsWithPostId(post_id)
  html = ''
  for row in rows:
    html += '<img src="' + str(row['imgpath']) + '"/>'
  return html

def getImagesLiWithPostId(post_id):
  rows = getImagePathsWithPostId(post_id)
  html = ''
  for row in rows:
    html += '<li><a href="' + str(row['imgpath']) + '"><img src="' + str(row['imgpath']) + '"/></a></li>'
  return html



def getAccessNum():
  num = 0
  conn = sqlite3.connect(DBNAME)
  c = conn.cursor()
  sql = 'select num from access_num'
  for row in c.execute(sql):
    num = row[0]
  update_sql = 'update access_num set num=' + str(num+1)
  c.execute(update_sql)
  conn.commit()
  conn.close()
  return str(num+1)



def addImageFile(data, post):
  #/static/uploads/year/month/date/
  return 'add image file'

def addNews(title, body, datetime, category, owner):
  return 'add news'

def getNews(id):
  return 'get news'

def removeNews(id):
  return 'remove news'

def editNews(id, title, body, date, category, owner):
  return 'edit news'

