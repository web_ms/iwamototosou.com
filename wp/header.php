<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78390012-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78390012-5');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
<title>岩本塗装 | 外壁塗装、屋根塗装のことなら静岡県函南町の岩本塗装。</title>
<meta name="description" content="岩本塗装は10年後、笑顔でお客様に会える様に、丁寧な説明・丁寧な施工・その先までお客様に寄り添う仕事をしております。私たちの「職人力」には自信があります。是非お任せください。">
<link rel="stylesheet" href="<?php t_url(); ?>/static/common.css">
<link rel="stylesheet" href="<?php t_url(); ?>/static/page.css">
<link rel="stylesheet" href="<?php t_url(); ?>/static/css/movies.css">
<link rel="stylesheet" href="<?php t_url(); ?>/static/media.css">
<link rel="stylesheet" href="<?php t_url(); ?>/static/print.css" media="print">
</head>
<body>

<header>
  <div class="inner">
    <div class="logo">
      <a href="<?php h_url(); ?>">
        <div class="text">
          <span class="description">外壁塗装、屋根塗装のことなら静岡県函南町の岩本塗装。</span><br>
          <span class="name_ja">岩本塗装</span>
          <span class="name_en">IWAMOTO<br>TOSOU</span>
        </div>
      </a>
    </div>
    <nav class="menu">
      <ul>
        <li><a href="<?php h_url(); ?>">ホーム</a></li>
        <li><a href="<?php h_url(); ?>/about/">岩本塗装について</a></li>
        <li><a href="<?php h_url(); ?>/works/">施工実績</a></li>
        <li><a href="<?php h_url(); ?>/flow/">工事の流れ</a></li>
        <li><a href="<?php h_url(); ?>/news/">塗装日誌</a></li>
        <li><a href="<?php h_url(); ?>/movies/">動画</a></li>
        <li><a href="#contact">お問合せ</a></li>
      </ul>
    </nav>
    <div class="menubutton">
      <img src="<?php t_url(); ?>/static/img/menu.png">
    </div>
  </div>
</header>

<div id="bottomright">
  <div class="banner">
    <a href="https://www.gaihekitosou-partners.jp/" target="_blank" rel="noreferrer noopener"><img src="https://www.gaihekitosou-partners.jp/wp-content/uploads/gaiheki-banner-300_100.jpg" alt="外壁塗装の見積りサイト認定の優良塗装店"></a>
  </div>
  <nav class="contact">
    <h1>[ お見積り・お問合せは ]</h1>
    <div class="box">
      <a class="tel" href="tel:055-956-0583"><em>055-956-0583</em><br>(10:00 〜 20:00)</a>
      <a class="mail" href="#contact"><img src="<?php t_url(); ?>/static/img/mail.png"></a>
    </div>
  </nav>
</div>


