<?php

function my_posts_news_footer(){
  $args = array(
    'post_type' => 'post_news',
    'posts_per_page' => 5,
    'orderby' => 'date',
    'order' => 'DESC',
  );
  $wq = new WP_Query($args);
  if( $wq->have_posts() ){
    echo '<ul>';
    while( $wq->have_posts() ){
      $wq->the_post();
      echo '<li>';
      echo '<a href="' . get_the_permalink() . '">';
      echo '<div class="head">';
      echo '<span class="date">' . get_the_time('Y.m.d') . '</span>';
      echo '<span class="title">' . get_the_title() . '</span>';
      echo '<span class="category">' . my_get_term_list(get_the_ID()) . '</span>';
      echo '</div>';
      echo '</a>';
      echo '</li>';
    }
    echo '</ul>';
  }
  wp_reset_postdata();
}

function my_posts_news_common($post_id=null){
  $args = array(
    'post_type' => 'post_news',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC',
  );
  if(!is_null($post_id)){
    $array['p'] = $post_id;
  }
  $wq = new WP_Query($args);
  if( $wq->have_posts() ){
    echo '<ul>';
    while( $wq->have_posts() ){
      global $post;
      $post = $wq->the_post();
      echo '<li>';
      echo '<a href="' . get_the_permalink() . '">';
      echo '<div class="head">';
      echo '<span class="date">' . get_the_time('Y.m.d') . '</span>';
      echo '<span class="title">' . get_the_title() . '</span>';
      echo '<span class="category">' . my_get_term_list(get_the_ID()) . '</span>';
      echo '</div>';
      echo '</a>';
      echo '<div class="body">';
      if( get_field('img') ){
        echo '<img src="' . get_field('img') . '">';
      }
      echo '<p>' . get_field('text') . '</p>';
      if( get_field('other_img01') ){
        echo '<img src="' . get_field('other_img01') . '">';
      }
      if( get_field('other_img02') ){
        echo '<img src="' . get_field('other_img01') . '">';
      }
      if( get_field('other_img03') ){
        echo '<img src="' . get_field('other_img01') . '">';
      }
      if( get_field('other_img04') ){
        echo '<img src="' . get_field('other_img01') . '">';
      }
      if( get_field('other_img05') ){
        echo '<img src="' . get_field('other_img01') . '">';
      }
      echo '<div class="contents">' . get_field('contents') . '</div>';
      echo '<div class="contents_default">' . get_the_content() . '</div>';
      echo '</div>';
      echo '</li>';
    }
    echo '</ul>';
  }
  wp_reset_postdata();
}

function my_posts_works(){
  $args = array(
    'post_type' => 'post_works',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC',
  );
  $wq = new WP_Query($args);
  if( $wq->have_posts() ){
    echo '<ul>';
    while( $wq->have_posts() ){
      $wq->the_post();
      echo '<a href="' . get_the_permalink() . '">';
      echo '<li>';
      echo '<span class="img"><img src="' . get_field('after_img') . '"></span>';
      echo '<span class="title">' . get_the_title() . '</span>';
      echo '<span class="month">' . get_field('month') . '</span>';
      echo '<span class="category">' . get_field('category_text') . '</span>';
      echo '<span class="text">' . get_field('text') . '</span>';
      echo '</li>';
      echo '</a>';
    }
    echo '</ul>';
  }
  wp_reset_postdata();
}

?>
