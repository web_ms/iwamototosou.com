<?php

/**
 * カスタム投稿タイプ、カスタム分類タイプを追加
 * $cat_name カテゴリタイプ名
 * $cat_label カテゴリメニュー表示
 * $cat_slug カテゴリアーカイブページのURLスラッグ
 * $post_name 投稿タイプ名
 * $post_label 投稿メニュー表示
 * $post_position 投稿メニュー表示位置
 * $post_slug 投稿シングルページのURLスラッグ
 */
function my_add_custom(
                    $cat_name, 
                    $cat_label,
                    $cat_slug,
                    $cat_show,
                    $post_has_archive,
                    $post_name,
                    $post_label,
                    $post_position,
                    $post_slug ){
    //カスタム投稿タイプ$post_nameに属するカスタム分類タイプ$cat_nameの追加
    register_taxonomy($cat_name,
        array($post_name),
        array(
            //管理画面ラベル名
            'label' => $cat_label,
            //管理画面に表示しない
            'show_ui' => $cat_show,
            //分類に親子関係を持たせる
            'hierarchical' => true,
            //分類アーカイブページのURLを　$cat_slug/category/スラッグ　とする
            'rewrite' => array('slug' => $cat_slug.'/category', 'with_front' => false)
    ));
    //カスタム投稿タイプ$post_nameの追加
    register_post_type($post_name, array(
        //管理画面ラベル名
        'label' => $post_label,
        //管理画面の表示位置
        'menu_position' => $post_position,
        //管理画面に表示しサイト上にも表示する
        'public' => true,
        //タイトル、内容、アイキャッチ画像、カスタムフィールドを使う
        'supports' => array('title', 'editor', 'thumbnail', 'custom-fields'),
        //アーカイブページを持たない（カスタム分類のアーカイブで表示する）
        'has_archive' => $post_has_archive,
        //シングルページのURLを　/スラッグ/%post_id%/　とする
        'rewrite' => array('slug' => $post_slug, 'with_front' => false)
    ));
}

function my_add_custom_tag(
                    $tag_name, 
                    $tag_label,
                    $tag_slug,
                    $tag_show,
                    $post_name ){
    //カスタム投稿タイプ$post_nameに属するカスタム分類タイプ$tag_nameの追加
    register_taxonomy($tag_name,
        array($post_name),
        array(
            //管理画面ラベル名
            'label' => $tag_label,
            //管理画面に表示しない
            'show_ui' => $tag_show,
            //分類に親子関係を持たせない（タグ）
            'hierarchical' => false,
            //分類アーカイブページのURLを　$tag_slug/tag/スラッグ　とする
            'rewrite' => array('slug' => $tag_slug.'/tag', 'with_front' => false)
    ));
}

function my_add_custom_works(){
    my_add_custom(
               'cat_works',
               '施工実績カテゴリ',
               'works',
               true,
               true,
               'post_works',
               '施工実績',
               5,
               'works');
}
add_action('init', 'my_add_custom_works');
function my_add_custom_news(){
    my_add_custom(
               'cat_news',
               '塗装日誌カテゴリ',
               'news',
               true,
               true,
               'post_news',
               '塗装日誌',
               5,
               'news');
}
add_action('init', 'my_add_custom_news');
function my_add_custom_movies(){
    my_add_custom(
               'cat_movies',
               '動画カテゴリ',
               'movies',
               true,
               true,
               'post_movies',
               '動画',
               5,
               'movies');
}
add_action('init', 'my_add_custom_movies');

//
// https://tips.adrec-dept.com/wordpress/1426/
//
function my_add_columns_works($columns) {
  $columns['img'] = 'img';
  $columns['author'] = 'author';
  return $columns;
}
add_filter( 'manage_edit-post_works_columns', 'my_add_columns_works' );
function my_add_columns_news($columns) {
  $columns['category'] = 'category';
  $columns['img'] = 'img';
  $columns['author'] = 'author';
  return $columns;
}
add_filter( 'manage_edit-post_news_columns', 'my_add_columns_news' );

function my_add_columns_content( $column_name, $post_id ) {
  $meta = get_post_meta( $post_id, $column_name, true );
  if( $column_name == 'category' ){
    echo my_get_term_list( $post_id );
  }
  else if( $column_name == 'auther' ){
    $stitle = get_the_auther();
  }
  else if( $column_name == 'img' ){
    if(get_field('img', $post_id)){
      $stitle = '<img src="' . get_field( 'img', $post_id ) . '" style="width:150px; height:auto;">';
    }
    else if(get_field('after_img', $post_id)){
      $stitle = '<img src="' . get_field( 'after_img', $post_id ) . '" style="width:150px; height:auto;">';
    }
    else if(my_get_first_image()){
      $stitle = '<img src="' . my_get_first_image() . '" style="width:150px; height:auto;">';
    }
  }
  if ( isset($stitle) && $stitle ) {
    echo $stitle;
  }
}
add_action( 'manage_posts_custom_column', 'my_add_columns_content', 10, 2 );

//
// https://ouchi-it.com/custom-columns/
//
function sort_posts_column_works($columns){
  $columns = array(
    'cb' => '<input type="checkbox" />',
    'title' => 'タイトル',
    'img' => '画像',
    'author' => '作成者',
    'date' => '日時',
  );
  return $columns;
}
add_filter( 'manage_edit-post_works_columns', 'sort_posts_column_works');

function sort_posts_column_news($columns){
  $columns = array(
    'cb' => '<input type="checkbox" />',
    'title' => 'タイトル',
    'img' => '画像',
    'category' => 'カテゴリ',
    'author' => '作成者',
    'date' => '日時',
  );
  return $columns;
}
add_filter( 'manage_edit-post_news_columns', 'sort_posts_column_news');

/**
 * pre_get_posts
 */
function change_query($query) {
  if( is_admin() || ! $query->is_main_query() ){
    return;
    }
  if( $query->is_tax() ){
    $query->set('posts_per_page', '-1');
  }
  if( $query->is_post_type_archive('post_news') ){
    $query->set('posts_per_page', '20');
    $query->set('orderby', 'date');
    $query->set('order', 'DESC');
    return;
  }
  if( $query->is_post_type_archive('post_works') ){
    $query->set('posts_per_page', '-1');
    $query->set('orderby', 'date');
    $query->set('order', 'DESC');
    return;
  }
}
add_action('pre_get_posts', 'change_query');

/**
 * my_get_posts
 */
function my_get_posts_html( $post_type ){
  $html = '';
  if( $post_type == 'post_news' ){
  }
  return $html;
}

// メニューの並び替え
function my_custom_menu_order($menu_order) {
  if (!$menu_order) return true;
  return array(
    'index.php',
    'edit.php?post_type=page',
    'separator1',
    'edit.php?post_type=post_works',
    'edit.php?post_type=post_news',
    'edit.php?post_type=post_movies',
    'separator2',
    'upload.php',
    'separator-last', //最後のセパレータ
    'themes.php', //外観
    'plugins.php', //プラグイン
    'users.php', //ユーザー
    'tools.php', //ツール
    'options-general.php', //設定
  );
}
add_filter('custom_menu_order', 'my_custom_menu_order');
add_filter('menu_order', 'my_custom_menu_order');

// customize admin menus
function remove_menus() {
  //remove_menu_page( 'index.php' ); // ダッシュボード
  remove_menu_page( 'edit.php' ); // 投稿
  //remove_menu_page( 'upload.php' ); // メディア
  //remove_menu_page( 'edit.php?post_type=page' ); // 固定
  remove_menu_page( 'edit-comments.php' ); // コメント
  //remove_menu_page( 'themes.php' ); // 外観
  //remove_menu_page( 'plugins.php' ); // プラグイン
  //remove_menu_page( 'users.php' ); // ユーザー
  //remove_menu_page( 'tools.php' ); // ツール
  //remove_menu_page( 'options-general.php' ); // 設定
}
add_action( 'admin_menu', 'remove_menus' ); 

//
// https://thewppress.com/libraries/change-the-excerpt-length-and-excerpt-more/
//
function my_change_excerpt_length( $length ) {
  return 100;
}
add_filter( 'excerpt_length', 'my_change_excerpt_length', 999 );
function my_change_excerpt_more( $more ) {
  $html = '<a href="' . esc_url( get_permalink() ) . '"> [...]</a>';
  return $html;
}
add_filter( 'excerpt_more', 'my_change_excerpt_more' );

function my_admin_style() {
  wp_enqueue_style( 'my_admin_style', get_stylesheet_directory_uri() . '/css/admin.css' );
}
add_action( 'admin_enqueue_scripts', 'my_admin_style' );

add_filter( 'get_the_excerpt', function( $excerpt ){
  return str_replace( '&nbsp', '', $excerpt );
});

function add_auto_default_term_post_news($post_ID) {
  $terms = wp_get_object_terms($post_ID, 'cat_news');
  if(count($terms)==0){
    wp_set_object_terms($post_ID, 'news', 'cat_news');
  }
}
add_action('publish_post_news', 'add_auto_default_term_post_news');

?>

