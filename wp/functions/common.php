<?php

function my_access_num(){
  $num = get_option('access_num');
  if($num){
    $num = intval($num);
    update_option($num);
  }
  else {
    $num = 1;
  }
  echo $num;
}

function my_shortcode_home_url($args){
  return home_url();
}
add_shortcode('home_url','my_shortcode_home_url');

function my_shortcode_template_part($args){
  $path = $args[0];
  if(my_exists_template_part($path)){
    echo my_template_part($path);
  }
}
add_shortcode('template_part','my_shortcode_template_part');

function my_exists_template_part($name){
  $locate = locate_template('parts/'.$name.'.php');
  if($locate){
    return true;
  }
  else{
    return false;
  }
}

function my_template_part($name, $args=null){
  if( isset($args) && $args != null ){
    global $part_args;
    $part_args = $args;
  }
  get_template_part('parts/'.$name);
}

function my_get_page_id($slug){
  $page = get_page_by_path($slug);
  return ($page===null) ? '' : $page->ID;
}

function my_get_page_slug(){
  if(is_page()){
    global $post;
    return $post->post_name;
  }
  else{
    return '';
  }
}

function my_css_ver() {
  echo '?ver=' . date('YmdHis');
}

function h_url() {
  echo home_url();
}

function get_h_url() {
  return home_url();
}

function t_url() {
  echo get_stylesheet_directory_uri();
}

function get_t_url() {
  return get_stylesheet_directory_uri();
}

function my_get_term_ID($cat_slug, $cat_type) {
  $term = get_term_by('slug', $cat_slug, $cat_type);
  return $term->term_id;
}

function my_get_term($cat_slug, $cat_type) {
  $term = get_term_by('slug', $cat_slug, $cat_type);
  return $term;
}

function my_get_term_list( $post_id, $cat_type = null, $is_link = false ){
  if(!$cat_type){
    $post_type = get_post($post_id)->post_type;
    $cat_type = str_replace('post_', 'cat_' ,$post_type);
  }
  $terms = get_the_terms( $post_id, $cat_type );
  $html = '';
  if( !empty($terms) && !is_wp_error($terms) ){
    $html = $html . '<ul>';
    foreach( $terms as $term ){
      if($is_link){
        $html = $html . '<li><a class="term" href="' . get_term_link( $term ) . '">' . $term->name . '</a></li>';
      }
      else{
        $html = $html . '<li class="term">' . $term->name . '</li>';
      }
    }
    $html = $html . '</ul>';
  }
  return $html;
}

function my_get_first_image() {
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches [1] [0];
	if(empty($first_img)){
		$first_img = get_t_url().'/img/common/noimage.png';;
	}
	return $first_img;
}

function my_get_image_for_list() {
	global $post, $posts;
  $r = '';
  if( get_field('img') ) $r = get_field('img');
  else $r =  my_get_first_image();
  return $r;
}

//
// from ymcon.com
//
function my_echo_archives_yearly($post_type, $suffix='', $show_count=false) {
  $html = '';
  if($show_count){
    $html = wp_get_archives( 'post_type='.$post_type.'&type=yearly&echo=0&show_post_count=1' );
    $html = str_replace('</a>', $suffix.'<small>', $html);
    $html = str_replace('</li>', '</small></a></li>', $html);
  }
  else{
    $html = wp_get_archives( 'post_type='.$post_type.'&type=yearly&echo=0' );
    $html = str_replace('</a>', '</a>' . $suffix, $html);
  }
  echo $html;
}

function my_echo_archives_monthly($post_type, $suffix='', $show_count=false) {
  $html = '';
  if($show_count){
    $html = wp_get_archives( 'post_type='.$post_type.'&type=monthly&echo=0&show_post_count=1' );
    $html = str_replace('</a>', $suffix.'<small>', $html);
    $html = str_replace('</li>', '</small></a></li>', $html);
  }
  else{
    $html = wp_get_archives( 'post_type='.$post_type.'&type=monthly&echo=0' );
    $html = str_replace('</a>', '</a>' . $suffix, $html);
  }
  echo $html;
}

function my_echo_categories($cat_type, $show_option_all='', $title_li=''){
  global $post;
  wp_list_categories(
    array(
      'taxonomy'=>$cat_type,
      'show_option_all'=>$show_option_all,
      'title_li'=>$title_li,
    )
  );
}

function my_get_breadcrumb( $slug, $page_title ){
  $front_page_id = get_option( 'page_on_front' );
  $front_page_title = get_the_title( $front_page_id );
  if(!$front_page_title) $front_page_title = 'TOP';
  // https://plusers.net/wordpress_pankuzulist
  $bc_arr = array( array('text'=>$front_page_title, 'url'=>home_url()) );
  if( is_tax() ){
    $bc_arr[] = array('text'=>$page_title, 'url'=>home_url().'/'.$slug.'/');
    $bc_arr[] = array('text'=>'カテゴリ：'.single_term_title(' ', false), 'url'=>'');
  }
  else if( is_single() ){
    global $post;
    $bc_arr[] = array('text'=>$page_title, 'url'=>home_url().'/'.$slug.'/');
    $bc_arr[] = array('text'=>single_post_title(' ', false), 'url'=>'');
  }
  else if( is_year() ){
    $bc_arr[] = array('text'=>$page_title, 'url'=>home_url().'/'.$slug.'/');
    $bc_arr[] = array('text'=>'アーカイブ：'.get_query_var('year').'年', 'url'=>'');
  }
  else{
    $bc_arr[] = array('text'=>$page_title, 'url'=>'');
  }
  $html = '';
  foreach( $bc_arr as $bc ){
    $text = $bc['text'];
    $url = $bc['url'];
    if( !is_null($url) && $url != '' ){
      $html .= '<a href="' . $url . '">' . $text . '</a>&nbsp;&gt;&nbsp;';
    }
    else{
      $html .= '<span>' . $text . '</span>';
    }
  }
  return $html;
}

function my_get_index_links( $page_id ){
  $html = '';
  for($i=1; $i<=5; $i++){
    $sec_id = 's0'.$i;
    $sec_title = get_field($sec_id.'_title');
    if( $sec_title ){
      $html .= '<div class="more"><a href="#' . $sec_id . '">' . $sec_title . '</a></div>';
    }
  }
  return $html;
}

function my_get_nav($mode = 'nav'){
  $html = '';
  if(!($mode=='nav'||$mode=='ul'||$mode=='li')) $mode = 'nav';
  $wq = new WP_Query(array(
    'post_type' => 'page',
    'orderby' => 'menu_order',
    'order' => 'ASC',
  ));
  if($wq->have_posts()){
    if($mode=='nav') $html.='<nav>';
    if(($mode=='ul'||$mode=='nav')) $html.='<ul>';
    while($wq->have_posts()){
      $wq->the_post();
      if(get_field('is_nav')){
        $url = get_the_permalink();
        $title = get_the_title();
        $html .= '<li><a href="' . $url . '">' . $title . '</a></li>';
      }
    }
    if(($mode=='ul'||$mode=='nav')) $html.='</ul>';
    if($mode=='nav') $html.='</nav>';
  }
  wp_reset_postdata();
  return $html;
}

function my_ajax_server() {
	$nonce = $_REQUEST['nonce'];
  if( wp_verify_nonce($nonce, 'my-ajax-nonce') ){
    $method = $_POST['method'];
    $args = $_POST['args'];
    if( isset($method) ){
      my_template_part($method, $args);
    }
  }
  die();
}
add_action( 'wp_ajax_my_ajax_server', 'my_ajax_server' );
add_action( 'wp_ajax_nopriv_my_ajax_server', 'my_ajax_server' );

?>

