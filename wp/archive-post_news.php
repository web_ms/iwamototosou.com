<?php get_header(); ?>

<div id="header_margin"></div>

<div id="mv" class="page delay">
  <div class="main">
    <img class="pc" src="<?php t_url(); ?>/static/img/news/title_news.jpg">
  </div>
</div><!--/#mv-->

<main class="news page">

<section id="s01"">
  <h2 class="delay">
    [岩本塗装 - 塗装日誌]<br>
    <span class="small">お知らせや塗装についてのことなどを発信します。</small>
  </h2>
  <div class="inner">
    <div class="leftimg delay delay-left"><img src="<?php t_url(); ?>/static/img/base01.png"></div>
    <div class="rightimg delay delay-right"><img src="<?php t_url(); ?>/static/img/base02.png"></div>
    <div class="page_inner">
      <div class="posts">
        <?php if( have_posts() ): ?>
        <ul>
          <?php while( have_posts() ): ?>
          <?php the_post(); ?>
          <li>
            <a href="<?php the_permalink(); ?>">
              <div class="head">
                <span class="date"><?php the_time('Y.m.d'); ?></span>
                <span class="title"><?php the_title(); ?></span>
                <span class="category"><?php echo my_get_term_list(get_the_ID()); ?></span>
              </div>
            </a>
            <div class="body">
              <?php if( get_field('img') ): ?>
              <img src="<?php the_field('img'); ?>">
              <?php endif; ?>
              <p><?php the_field('text'); ?></p>
              <?php for($i=1;$i<=5;$i++): ?>
              <?php if( get_field('other_img0'.$i) ): ?>
              <img src="<?php the_field('other_img0'.$i); ?>">
              <?php endif; ?>
              <?php endfor; ?>
              <div class="contents"><?php the_field('contents'); ?></div>
              <div class="contents_default"><?php the_content(); ?></div>
            </div>
          </li>
          <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <aside>
        <p class="title">CATEGORY</p>
        <ul>
          <?php
            wp_list_categories(array(
              'taxonomy' => 'cat_news',
              'show_count' => 0,
              'title_li' => '',
              )
            );
          ?>
        </ul>
        <p class="title">ARCHIVES</p>
        <ul>
          <?php my_echo_archives_monthly('post_news'); ?>
        </ul>
      </aside>
    </div>
  </div>
</section>

<section class="area delay delay-bottom">
  <h2>[対応エリア]</h2>
  <div class="inner">
    <div class="left">
      <img src="<?php t_url(); ?>/static/img/area.png">
    </div>
    <div class="right">
      <p>
        静岡県東部全域<br>
        沼津市、三島市、熱海市、伊東市、御殿場市、<br>
        裾野市、伊豆市、伊豆の国市、函南町、清水町、<br>
        長泉町、小山町、富士市、富士宮市<br>
        <span class="small">※その他のエリアも対応いたします。お気軽にご相談ください。</span><br>
      </p>
    </div>
  </div><!--/.inner-->
</section>

</main>

<?php get_footer(); ?>
