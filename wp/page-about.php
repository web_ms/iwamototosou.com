<?php get_header(); ?>

<div id="header_margin"></div>

<div id="mv" class="page delay">
  <div class="main">
    <img class="pc" src="<?php t_url(); ?>/static/img/about/title_about.jpg">
  </div>
</div><!--/#mv-->

<main class="about page">

<section id="s01">
  <h2 class="delay">
    [岩本塗装-サービスのご案内]<br>
    <span class="small">
      一般建築塗装から防水、塗り替えまで。塗装に関して何でも承ります。<br>
      まずはお気軽にご連絡ください。
    </span>
  </h2>
  <div class="inner delay delay-bottom">
    <div class="leftimg delay delay-left"><img src="<?php t_url(); ?>/static/img/base01.png"></div>
    <div class="rightimg delay delay-right"><img src="<?php t_url(); ?>/static/img/base02.png"></div>
    <div class="page_inner">
      <h3>01.外壁塗装</h3>
      <p>
        外壁塗装には、外観をきれいに見せるほか、雨水や紫外線などから家を守るための役割があります。<br>
        その分、劣化しやすく定期的に塗り替えが必要になります。<br>
        通常であれば、10年程度を目安に塗り替えをおすすめしております。<br>
        <img src="<?php t_url(); ?>/static/img/about/about01.jpg"/>
      </p>
      <h3>02.屋根塗装</h3>
      <p>
        屋根塗装にも、雨風や直射日光から建物を守るとても重要な役割があります。<br>
        家の中で最も劣化しやすい箇所です。<br>
        常に外部と触れ合っている箇所でもあるため、定期的に塗り替え・メンテナンスを行い、<br>
        早めの屋根塗装で長持ちさせ丈夫な状態を保つことが大切です。<br>
        <img src="<?php t_url(); ?>/static/img/about/about02.jpg"/>
      </p>
      <h3>03.吹付け</h3>
      <p>
        外壁塗装の塗布は数回重ね塗りをしますが美しい外観のために仕上げの塗装を行います。<br>
        仕上げのひとつが吹き付け塗装です。スプレーなどで塗料を吹き付けることです。<br>
        <img src="<?php t_url(); ?>/static/img/about/about03.jpg"/>
      </p>
      <h3>04.防水塗装</h3>
      <p>
        ビルの屋上などベランダの床や屋上といった場所に施工する工事です。<br>
        平面の為水はけが悪く、ひび割れなどを放っておくと雨漏りの原因となってしまいます。<br>
        それぞれの建物にあった防水工事をいたします。<br>
      </p>
      <h3>05.シール工事</h3>
      <p>
        外壁のボードとボードのつなぎ目（隙間）をシーリング材で埋める工事です。<br>
        防水効果、耐震効果があるのでひび割れや隙間など気になるところは<br>
        お気軽にご相談ください。お早目の工事をおすすめします。<br>
        <img src="<?php t_url(); ?>/static/img/about/about05.jpg"/>
      </p>
      <h3>06.その他塗り替え工事</h3>
      <p>
        その他塗装に関してのことならば、なんでもご相談ください。<br>
      </p>
    </div><!--/.page_inner-->
  </div><!--/.inner-->
</section>

<section id="s02" class="delay delay-bottom">
  <div class="bg">
    <img src="<?php t_url(); ?>/static/img/about/kihon_base.jpg">
  </div>
  <h2>[基本情報]</h2>
  <div class="inner">
    <table>
      <tr><th>屋号</th><td>岩本塗装</td></tr>
      <tr><th>代表者</th><td>岩本一道</td></tr>
      <tr><th>住所</th><td>〒419-0107 静岡県田方郡函南町平井32-11</td></tr>
      <tr><th>TEL/FAX</th><td>055-956-0583</td></tr>
      <tr><th>職種</th><td>塗装業</td></tr>
    </table>
  </div><!--/.inner-->
</section>

<section class="area delay delay-bottom">
  <h2>[対応エリア]</h2>
  <div class="inner">
    <div class="left">
      <img src="<?php t_url(); ?>/static/img/area.png">
    </div>
    <div class="right">
      <p>
        静岡県東部全域<br>
        沼津市、三島市、熱海市、伊東市、御殿場市、<br>
        裾野市、伊豆市、伊豆の国市、函南町、清水町、<br>
        長泉町、小山町、富士市、富士宮市<br>
        <span class="small">※その他のエリアも対応いたします。お気軽にご相談ください。</span><br>
      </p>
    </div>
  </div><!--/.inner-->
</section>

</main>

<?php get_footer(); ?>
