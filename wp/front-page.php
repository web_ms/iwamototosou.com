<?php get_header(); ?>

<div id="mv" class="top delay">
  <div class="main">
    <ul>
      <li class="mv02">
        <img class="pc" src="<?php t_url(); ?>/static/img/mv02.jpg">
        <img class="sp" src="<?php t_url(); ?>/static/img/spmv02.jpg">
      </li>
      <li class="mv01">
        <img class="pc" src="<?php t_url(); ?>/static/img/mv01.png">
        <img class="sp" src="<?php t_url(); ?>/static/img/sp_mv.png">
        <p class="delay delay-bottom">
          <span class="en">IWAMOTO</span>
          <span class="ja">塗装のホームドクター、岩本塗装</span>
          <span class="en">TOSOU</span>
        </p>
      </li>
    </ul>
  </div>
  <div class="pagenation">
    <span class="circle current">
    <span class="circle">
    <span class="circle">
    <span class="circle">
    <span class="circle">
  </div>
</div><!--/#mv-->

<main class="top">

<section id="s01">
  <div class="inner">
    <div class="leftimg delay delay-left"><img src="<?php t_url(); ?>/static/img/base01.png"></div>
    <div class="rightimg delay delay-right"><img src="<?php t_url(); ?>/static/img/base02.png"></div>
    <p class="summary delay delay-top">
      岩本塗装は<br>
      10年後、笑顔でお客様に会える様に、<br>
      丁寧な説明・丁寧な施工・その先まで<br>
      お客様に寄り添う仕事をしております。<br>
      私たちの「職人力」には自信があります。<br>
      是非お任せください。<br>
    </p>
    <div class="col2">
      <div class="left">
        <img src="<?php t_url(); ?>/static/img/ph01.png">
      </div>
      <div class="right">
        <h2>岩本塗装のこだわり</h2>
        <p>
          岩本塗装は他社施工の材料希釈率との違いにより、<br>
          材料本来の耐候性・耐久性をもたせる施工をしています。<br>
          施工前も<b>カラーシミュレーション</b>をご用意、<br>
          お客様が納得していただけるよう丁寧に説明いたします。<br>
          静岡県東部を中心に展開しております。<br>
          お気軽にお問合せください。
        </p>
      </div>
    </div>
  </div><!--/.inner-->
</section>

<section id="s02" class="delay delay-bottom">
  <div class="bg"><img src="<?php t_url(); ?>/static/img/simulation_base.png"></div>
  <div class="inner">
    <div class="front">
      <h1>岩本塗装のこだわり</h1>
      <div>
        <h3>[01.カラーシミュレーション]</h3>
        <p>
          工事を頼んでみたもののカラーを悩まれる方は非常に多いです。<br>
          サンプルを見るだけではイメージしにくいお客様の為に<br>
          カラーシミュレーションをご用意いたします（別途料金がかかります）<br>
        </p>
      </div>
      <table>
        <tr>
          <td></td>
          <td><figure><img src="<?php t_url(); ?>/static/img/simulation01.png"><figcaption>[工事前の建物写真]</figcaption></figure></td>
          <td></td>
        </tr>
        <tr>
          <td><figure><img src="<?php t_url(); ?>/static/img/simulation02.png"><figcaption>[シミュレーション01]</figcaption></figure></td>
          <td><figure><img src="<?php t_url(); ?>/static/img/simulation03.png"><figcaption>[シミュレーション02]</figcaption></figure></td>
          <td><figure><img src="<?php t_url(); ?>/static/img/simulation04.png"><figcaption>[シミュレーション03]</figcaption></figure></td>
        </tr>
      </table>
      <p>2色でスタイリッシュに表現できます</p>
      <table>
        <tr>
          <td><figure><img src="<?php t_url(); ?>/static/img/ph04.png"><figcaption>[シミュレーション04]</figcaption></figure></td>
          <td><figure><img src="<?php t_url(); ?>/static/img/ph05.png"><figcaption>[シミュレーション05]</figcaption></figure></td>
          <td><figure><img src="<?php t_url(); ?>/static/img/ph06.png"><figcaption>[シミュレーション06]</figcaption></figure></td>
        </tr>
      </table>
    </div><!--/.front-->
  </div><!--/.inner-->
</section>

<section id="s03">
  <div class="inner">
    <div class="right delay delay-right">
      <img src="<?php t_url(); ?>/static/img/ph02.png">
    </div><!--/.right-->
    <div class="left delay delay-left">
      <h3>[02.下地-下地調整剤の大切さ]</h3>
      <p>
        下地調整材ーこの材料の塗布量によって全てが決まると言っても過言ではありません。女性のお化粧時の化粧下地です。外壁の種類や劣化具合によって変えなければいけないのです。塗り方や希釈率の差により外壁のもちが変わります。是非ご相談ください。
      </p>
      <h3 class="a_right">[03.上塗り-保護剤]</h3>
      <p>
        保護剤ー下地調整材をしっかりと入れた後に保護膜を下塗りの上に塗布します。女性のお化粧時のファンデーションです。紫外線・雨・ホコリなどから外壁を守ります。色々な上塗りの種類もありますが耐久性とコストパフォーマンス的に溶剤のシリコンをオススメ致します。
      </p>
      <a class="more" href="<?php h_url(); ?>/about/">岩本塗装について詳しくみる</a>
    </div><!--/.left-->
  </div><!--/.inner-->
</section>

<section id="s04">
  <div class="inner">
    <div class="left delay delay-left">
      <a href="<?php h_url(); ?>/works/">
        <img src="<?php t_url(); ?>/static/img/works.png">
      </a>
    </div><!--/.left-->
    <div class="right delay delay-right">
      <a href="<?php h_url(); ?>/flow/">
        <img src="<?php t_url(); ?>/static/img/flowofwork.png">
      </a>
    </div><!--/.right-->
  </div><!--/.inner-->
</section>

</main>

<?php get_footer(); ?>
