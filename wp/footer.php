<footer>

<section id="news" class="delay delay-bottom">
  <div class="inner">
    <div class="left">
      <img class="ph" src="<?php t_url(); ?>/static/img/20210107_112447.jpg">
      <!--
      <img class="ph" src="<?php t_url(); ?>/static/img/20200801_065804.jpg">
      <img class="ph" src="<?php t_url(); ?>/static/img/topstaff_ph.png">
      -->
      <img class="text" src="<?php t_url(); ?>/static/img/news.png">
    </div><!--/.left-->
    <div class="right">
      <h2>[塗装日誌]</h2>
      <div class="posts">
        <?php my_posts_news_footer(); ?>
      </div>
    </div><!--/.right-->
  </div><!--/.inner-->
</section>

<section id="contact" class="contact delay delay-bottom">
  <div class="inner">
    <h2>[お問合せ]</h2>
    <p>
      ご相談からお見積もりまでお気軽にお問合せください。<br>
      丁寧に対応いたします。
    </p>
    <div class="form">
      <form>
        <table>
          <tr><th><label for="c_name">お名前:</label></th><td><input type="text" name="c_name" id="c_name"></tr>
          <tr><th><label for="c_email">Email:</label></th><td><input type="email" name="c_email" id="c_email"></tr>
          <tr><th><label for="c_tel">電話番号:</label></th><td><input type="tel" name="c_tel" id="c_tel"></tr>
          <tr><th><label for="c_address">住所:</label></th><td><input type="text" name="c_address" id="c_address"></tr>
          <tr><th><label for="c_content">お問い合わせ内容:</label></th><td><textarea name="c_content" id="c_content"></textarea></tr>
        </table>
        <div class="submit">
          <input type="button" name="submit" value="送信" id="c_submit">
        </div>
      </form>
    </div>
  </div>
</section>

<section class="map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d981.940212240554!2d138.9651419000907!3d35.10317628088041!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601996d5de242433%3A0x91019ac56454bb4c!2z44CSNDE5LTAxMDcg6Z2Z5bKh55yM55Sw5pa56YOh5Ye95Y2X55S65bmz5LqV77yT77yS4oiS77yR77yR!5e1!3m2!1sja!2sjp!4v1545370042832" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>

<section class="banner">
  <h2>- 協賛各社ホームページ -<br><br></h2>
  <ul>
    <li><a target="_blank" href="http://www.den-maru.com"><img src="<?php t_url(); ?>/static/img/denmaru_banner.png"></li>
    <li><a target="_blank" href="http://www.osumi-kensetu.com"><img src="<?php t_url(); ?>/static/img/osumi_banner.png"></li>
  </ul>
</section>

<section class="nav">
  <nav>
    <ul>
      <li><a href="<?php h_url(); ?>">ホーム</a></li>
      <li><a href="<?php h_url(); ?>/about/">岩本塗装について</a></li>
      <li><a href="<?php h_url(); ?>/works/">施工実績</a></li>
      <li><a href="<?php h_url(); ?>/flow/">工事の流れ</a></li>
      <li><a href="<?php h_url(); ?>/news/">塗装日誌</a></li>
      <li><a href="#contact">お問合せ</a></li>
    </ul>
  </nav>
  <div class="access_num">
    Total Access : <?php my_access_num(); ?>
  </div>
  <div class="copyright">
    © Iwamoto Tosou All Rights Reserved.
  </div>
</section>

</footer>
    
<div class="pagetop">
  <a href="#"><img src="<?php t_url(); ?>/static/img/back.png"></a>
</div>

</style>
<script>
  var src_menu_w = '<?php t_url(); ?>/static/img/menu_w.png';
  var src_menu = '<?php t_url(); ?>/static/img/menu.png';
</script>
<script type="text/javascript" src="<?php t_url(); ?>/static/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php t_url(); ?>/static/script.js"></script>

</body>
</html>


