<?php get_header(); ?>

<div id="header_margin"></div>

<div id="mv" class="page delay">
  <div class="main">
    <img class="pc" src="<?php t_url(); ?>/static/img/works/title_works.jpg">
  </div>
</div><!--/#mv-->

<main class="works page">

<section id="s01">
  <div class="inner delay delay-bottom">
    <div class="leftimg delay delay-left"><img src="<?php t_url(); ?>/static/img/base01.png"></div>
    <div class="rightimg delay delay-right"><img src="<?php t_url(); ?>/static/img/base02.png"></div>
    <div class="page_inner works_list">
      <h2 class="delay">[施工実績一覧]</h2>
      <p>写真をクリックすると、施工実績の詳細をご覧いただけます。</p>
      <?php my_posts_works(); ?>
    </div><!--/.page_inner-->
  </div><!--/.inner-->
</section>

<section class="area delay delay-bottom">
  <h2>[対応エリア]</h2>
  <div class="inner">
    <div class="left">
      <img src="<?php t_url(); ?>/static/img/area.png">
    </div>
    <div class="right">
      <p>
        静岡県東部全域<br>
        沼津市、三島市、熱海市、伊東市、御殿場市、<br>
        裾野市、伊豆市、伊豆の国市、函南町、清水町、<br>
        長泉町、小山町、富士市、富士宮市<br>
        <span class="small">※その他のエリアも対応いたします。お気軽にご相談ください。</span><br>
      </p>
    </div>
  </div><!--/.inner-->
</section>

</main>

<?php get_footer(); ?>

