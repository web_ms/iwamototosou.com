$(window).on('scroll load reload resize', function(){
  $('.delay').each(function(){
    if(!$(this).hasClass('show')){
      if($(this).offset().top - 600 + (($(this).offset().left)/($(document).width()))*100 < $(window).scrollTop()){
        $(this).addClass('show');
      }
    }
  });
  if ($(this).scrollTop() > 100) {
    $('header').addClass('thin');
    $('.menubutton img').attr('src',src_menu_w);
  }
  if ($(this).scrollTop() <= 100 && $('main').hasClass('top')) {
    $('header').removeClass('thin');
    $('.menubutton img').attr('src',src_menu);
  }
  if(!$('main').hasClass('top')){
    $('#header_margin').height($('header').height());
  }
});
$(window).scroll(function(){
  if ($(this).scrollTop() > 500) {
    $('div.pagetop').fadeIn(200);
  }
  if ($(this).scrollTop() <= 500) {
    $('div.pagetop').fadeOut(200);
  }
});
function scroll(){
  var speed = 500;
  var href= $(this).attr("href").replace("<?php echo the_permalink(); ?>", "");
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top - 80;
  $("html, body").animate({scrollTop:position}, speed, "swing");
  return false;
}
$('a[href^="#"]').click(scroll);
$(window).scroll(function(){
  if ($(this).scrollTop() > 500) {
    $('div.pagetop').fadeIn(200);
  }
  else {
    $('div.pagetop').fadeOut(200);
  }
});
$(document).ready(function(){
  $('header nav.menu ul li a').each(function(){
    if($(this).attr('href') == location.pathname){
      $(this).parent().addClass('current');
    }
  });
  if(!$('main').hasClass('top')){
    $('header').addClass('thin');
    $('.menubutton img').attr('src',src_menu_w);
  }
  var interval = 8000;
  var speed = 500;
  setInterval(function(){
    $('.mv01').fadeToggle(speed);
  }, interval);
});
$('.menubutton').click(function(){
  $('header nav').toggle(200);
});





/* admin */

function callapi(action, postdata, xmlhr, ok_func, ng_func){
  var api_url = '/' + action + '/';
  xmlhr.onreadystatechange = function(){
    if(xmlhr.readyState === 4){
      if(xmlhr.status == 200){
        ok_func();
      }
      else{
        ng_func();
      }
    }
  }
  xmlhr.onload = function(){
  }
  xmlhr.open('POST', api_url, true);
  xmlhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
  xmlhr.send(postdata);
}

function sendMail(name, email, tel, address, content, callback){
  var action = 'send_mail';
  var xmlhr = new XMLHttpRequest();
  var json_d = {
    'name' : name,
    'email' : email,
    'tel' : tel,
    'address' : address,
    'content' : content
  }
  var postdata = [
    'action=' + action,
    'json=' + JSON.stringify(json_d)
  ].join('&');
  ok_func = function() {
    var response = xmlhr.responseText;
    alert('メールを送信しました。お問い合わせありがとうございます。\n\n' + response);
    callback();
  }
  ng_func = function() {
    alert('送信に失敗しました');
  }
  callapi(action, postdata, xmlhr, ok_func, ng_func);
}

function clearContact(){
  document.querySelector('#c_name').value = '';
  document.querySelector('#c_email').value = '';
  document.querySelector('#c_tel').value = '';
  document.querySelector('#c_address').value = '';
  document.querySelector('#c_content').value = '';
}

function handleContactSubmit(e){
  var name = document.querySelector('#c_name').value;
  var email = document.querySelector('#c_email').value;
  var tel = document.querySelector('#c_tel').value;
  var address = document.querySelector('#c_address').value;
  var content = document.querySelector('#c_content').value;
  var error_text = '';
  if(name == '') error_text += '・お名前を入力してください\n';
  if(email == '') error_text += '・E-mailを入力してください\n';
  if(tel == '') error_text += '・電話番号を入力してください\n';
  if(address == '') error_text += '・住所を入力してください\n';
  if(address == '') error_text += '・お問い合わせ内容を入力してください\n';
  if(error_text != ''){
    alert('ERROR!\n\n' + error_text);
  }
  else {
    sendMail(name, email, tel, address, content ,clearContact);
  }
}
document.querySelector('#c_submit').addEventListener('click', handleContactSubmit, false);


