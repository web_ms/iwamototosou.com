<?php get_header(); ?>

<div id="header_margin"></div>

<div id="mv" class="page delay">
  <div class="main">
    <img class="pc" src="<?php t_url(); ?>/static/img/works/title_works.jpg">
  </div>
</div><!--/#mv-->

<main class="works page">

<section id="s01">
  <div class="inner delay delay-bottom">
    <div class="leftimg delay delay-left"><img src="<?php t_url(); ?>/static/img/base01.png"></div>
    <div class="rightimg delay delay-right"><img src="<?php t_url(); ?>/static/img/base02.png"></div>
    <div class="page_inner works_detail">
      <?php if(have_posts()): ?>
      <?php while(have_posts()): ?>
      <?php the_post(); ?>
      <div class="main">
        <h2 class="delay">[<?php the_title(); ?>]</h2>
        <table>
          <tr>
            <th>[Before]</th>
            <th></th>
            <th>[After]</th>
          </tr>
          <tr>
            <td>
              <a href="<?php the_field('before_img'); ?>">
                <img src="<?php the_field('before_img'); ?>">
              </a>
            </td>
            <td>
              <img src="<?php t_url(); ?>/static/img/works/arrow.png">
            </td>
            <td>
              <a href="<?php the_field('after_img'); ?>">
                <img src="<?php the_field('after_img'); ?>">
              </a>
            </td>
          </tr>
        </table>
      </div>
      <div class="photos">
        <h3>[PHOTO]</h3>
        <ul>
          <?php for($i=1;$i<=5;$i++): ?>
          <?php if(get_field('other_img0'.$i)): ?>
          <li>
            <a href="<?php the_field('other_img0'.$i); ?>">
              <img src="<?php the_field('other_img0'.$i); ?>"/>
            </a>
          </li>
          <?php endif; ?>
          <?php endfor; ?>
        </ul>
      </div>
      <div class="data">
        <h3>[ 施工データ ]</h3>
        <span class="title"><?php the_title(); ?></span><br>
        <span class="month"><?php the_field('month'); ?></span>
        <span class="category"><?php the_field('category_text'); ?></span><br>
        <span class="text"><?php the_field('text'); ?></span>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
      <?php wp_reset_postdata(); ?>
    </div><!--/.page_inner-->
  </div><!--/.inner-->
</section>

<section class="area delay delay-bottom">
  <h2>[対応エリア]</h2>
  <div class="inner">
    <div class="left">
      <img src="<?php t_url(); ?>/static/img/area.png">
    </div>
    <div class="right">
      <p>
        静岡県東部全域<br>
        沼津市、三島市、熱海市、伊東市、御殿場市、<br>
        裾野市、伊豆市、伊豆の国市、函南町、清水町、<br>
        長泉町、小山町、富士市、富士宮市<br>
        <span class="small">※その他のエリアも対応いたします。お気軽にご相談ください。</span><br>
      </p>
    </div>
  </div><!--/.inner-->
</section>

</main>

<?php get_footer(); ?>
